# PoC reverse shell with WSL

Taking advantage WSL processes are not as supervice as Windows host processes, I designed a malware that pretends to be a Word document, but it start a reverse shell trough WSL if it's installed.

## Usage
The victim needs the .lnk file and, in the same path, the /content folder. You can send it as a ZIP file, so the victim probably extracts everything in the same path.

![](evidence.gif)

## Support & Contributing
I you find any bug or you have a suggestion, let me know:

🐛 [Add issue](https://gitlab.com/mor88888888/poc-reverse-shell-with-wsl/-/issues/new)

📬 mor8@protonmail.com

## Roadmap 🚧
* Find what path are avaliable for Word or Wordpad and execute it
* Evading Defender AV. It detects the VBS file

## Authors and acknowledgment
Author: @mor88888888
